defmodule HangmanCli do
  alias HangmanCli.Interact

  defdelegate start, to: Interact
end
