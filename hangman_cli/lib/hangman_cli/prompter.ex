defmodule HangmanCli.Prompter do
  alias HangmanCli.{State}

  def accept_move(game = %State{}) do
    IO.gets("Your guess: ")
    |> check_input(game)
  end

  defp check_input({:error, reason}, _) do
    exit_with_message("Game ended: #{reason}")
  end

  defp check_input(:eof, _) do
    exit_with_message("Looks like you gave up...")
  end

  defp check_input(input, game = %State{}) do
    input = String.trim(input)

    cond do
      input =~ ~r/\A[a-z]\z/ ->
        %State{game | guess: input}

      true ->
        IO.puts("Please enter a single lowercase letter.")
        accept_move(game)
    end
  end

  defp exit_with_message(msg) do
    IO.puts(msg)
    exit(:normal)
  end
end
