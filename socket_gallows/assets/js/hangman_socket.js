import { Socket } from 'phoenix';

export default class HangmanSocket {
  constructor(tally) {
    this.tally = tally;
    this.socket = new Socket('/socket', {});
    this.socket.connect();
  }

  connectToHangman() {
    this.init();

    this.channel.on('tally', tally => {
      this.copy_tally(tally);
    });
  }

  init() {
    this.setupChannel();
    this.handleErrors();
  }

  setupChannel() {
    this.channel = this.socket.channel('hangman:game', {});
    this.channel
      .join()
      .receive('ok', resp => {
        console.log('connected:', resp);
        this.fetchTally();
      })
      .receive('error', err => {
        alert('Unable to join', err);
        console.error(`Error: ${err}`);
        throw err;
      });
  }

  fetchTally() {
    this.channel.push('tally', {});
  }

  make_move(guess) {
    this.channel.push('make_move', guess);
  }

  new_game() {
    this.channel.push('new_game', {});
  }

  copy_tally(from) {
    for (let k in from) {
      this.tally[k] = from[k];
    }
  }

  handleErrors() {
    this.channel.on('error', ({ error }) => console.error(error));
  }
}
