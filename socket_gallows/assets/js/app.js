// We need to import the CSS so that webpack will load it.
// The MiniCssExtractPlugin is used to separate it out into
// its own CSS file.
// import Vue from 'vue';
import sass from '../css/app.scss';

// window.Vue = Vue;
window.Vue = require('vue/dist/vue.js');

import 'phoenix_html';

import './hangman_app';
