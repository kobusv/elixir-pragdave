defmodule Dictionary do
  alias Dictionary.WordList

  defdelegate get_random_word(), to: WordList
  defdelegate start_link(), to: WordList
  defdelegate start(), to: WordList, as: :word_list

  def random_word(), do: get_random_word()
end
