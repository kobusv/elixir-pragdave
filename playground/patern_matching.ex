defmodule PaternMatching do
  def swap({a, b}) do: {b, a}
  def isequal({a, a}) do: true
  def isequal({a, b}) do: false
end
