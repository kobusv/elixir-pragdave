import HangmanSocket from './hangman_socket.js';

const RESPONSES = {
  won: ['success', 'You Won!'],
  lost: ['danger', 'You Lost!'],
  good_guess: ['success', 'Good guess!'],
  bad_guess: ['warning', 'Bad guess!'],
  already_used: ['info', 'You already guessed that'],
  initializing: ['info', "Let's Play!"],
};

const view = function(hangman) {
  const app = new Vue({
    el: '#app',
    data: {
      tally: hangman.tally,
    },

    computed: {
      game_over() {
        console.log(this.tally);
        return ['won', 'lost'].includes(this.tally.game_state);
      },
      game_state_message() {
        return RESPONSES[this.tally.game_state][1];
      },
      game_state_class() {
        return RESPONSES[this.tally.game_state][0];
      },
    },

    methods: {
      guess(char) {
        hangman.make_move(char);
      },
      new_game() {
        hangman.new_game();
      },
      already_guessed(char) {
        return this.tally.letters_used.includes(char);
      },
      correct_guess(char) {
        return this.already_guessed(char) && this.tally.letters.includes(char);
      },
      turns_gt(left) {
        return this.tally.turns_left > left;
      },
    },
  });
  return app;
};

window.onload = function() {
  const tally = {
    turns_left: 7,
    game_state: 'initializing',
    letters: ['_'],
    letters_used: '',
  };

  const hangman = new HangmanSocket(tally);
  const app = view(hangman);
  hangman.connectToHangman();
};
