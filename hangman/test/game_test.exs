defmodule GameTest do
  use ExUnit.Case

  alias Hangman.Game

  test "new_game returns structure" do
    game = Game.new_game()

    assert game.turns_left == 7
    assert game.game_state == :initializing
    assert length(game.letters) > 0

    # Assert that each item in the game.letters list is a string
    assert Enum.reduce(game.letters, true, fn x, acc -> acc && is_binary(x) end)
  end

  test "state has not changed if the game is already :won" do
    for state <- [:won, :lost] do
      game = Game.new_game() |> Map.put(:game_state, state)
      assert {^game, _} = Game.make_move(game, "z")
    end
  end

  test "first occurrence of letter is not already used." do
    game = Game.new_game()
    {game, _} = Game.make_move(game, "x")
    assert game.game_state != :already_used
  end

  test "Second occurrence of letter is already used." do
    game = Game.new_game()
    {game, _} = Game.make_move(game, "x")
    assert game.game_state != :already_used

    {game, _} = Game.make_move(game, "x")
    assert game.game_state == :already_used
  end

  test "a good guess is recognized" do
    game = Game.new_game("wibble")
    {game, _} = Game.make_move(game, "w")

    assert game.game_state == :good_guess
    assert game.turns_left == 7
  end

  def assert_moves(moves, game) do
    Enum.reduce(moves, game, fn {guess, state}, game ->
      {game, _} = Game.make_move(game, guess)
      assert game.game_state == state
      game
    end)
  end

  test "game is won" do
    moves = [
      {"w", :good_guess},
      {"i", :good_guess},
      {"b", :good_guess},
      {"l", :good_guess},
      {"e", :won}
    ]

    assert_moves(moves, Game.new_game("wibble"))
  end

  test "game is lost" do
    moves = [
      {"s", :bad_guess},
      {"t", :bad_guess},
      {"u", :bad_guess},
      {"b", :bad_guess},
      {"c", :bad_guess},
      {"h", :bad_guess},
      {"i", :lost}
    ]

    assert_moves(moves, Game.new_game("random"))
  end

  test "bad guess is recognised, and turns_left is decremented" do
    game = Game.new_game("wibble")
    {game, _} = Game.make_move(game, "x")
    assert game.game_state == :bad_guess
    assert game.turns_left == 6
  end
end
