defmodule GallowsWeb.HangmanView do
  use GallowsWeb, :view

  import Gallows.Views.Helpers.GameStateHelper

  def turn_class_name(left, target) when target >= left, do: ""
  def turn_class_name(_left, _target), do: "feint"

  def new_game_button(conn) do
    button("New Game", to: Routes.hangman_path(conn, :create_game))
  end

  def game_over?(%{game_state: game_state}), do: game_state in [:won, :lost]

  def format_letters(letters), do: Enum.join(letters, " ")
  def format_used_letters(str), do: str |> String.split("") |> Enum.join(" ")
end
