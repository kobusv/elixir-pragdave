defmodule HangmanCli.Summary do
  alias HangmanCli.State

  def display(game = %State{tally: tally}) do
    IO.puts([
      "\n",
      "Word so far: #{Enum.join(tally.letters, " ")}\n",
      "Guesses left: #{tally.turns_left}\n",
      "Letters used: #{tally.letters_used}"
    ])

    game
  end
end
