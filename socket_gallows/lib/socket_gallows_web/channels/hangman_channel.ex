defmodule SocketGallowsWeb.HangmanChannel do
  require Logger
  use Phoenix.Channel

  def join("hangman:game", _, socket) do
    game = Hangman.new_game()
    socket = assign(socket, :game, game)
    {:ok, socket}
  end

  def handle_in("tally", _, socket) do
    game = socket.assigns.game
    tally = Hangman.tally(game)
    push(socket, "tally", tally)
    {:noreply, socket}
  end

  def handle_in("new_game", _, socket) do
    socket = socket |> assign(:game, Hangman.new_game())
    handle_in("tally", nil, socket)
  end

  def handle_in("make_move", guess, socket) do
    tally = socket.assigns.game |> Hangman.make_move(guess)
    push(socket, "tally", tally)
    {:noreply, socket}
  end

  def handle_in(msg, _, socket) do
    error = "Invalid message received. Received: #{msg}"

    error
    |> log_error()
    |> return_error(socket)
  end

  def log_error(error) do
    Logger.error(error)
    error
  end

  def return_error(error, socket) do
    push(socket, "error", %{error: error})
    {:noreply, socket}
  end
end
