defmodule Playground do
  def swap({a, b}), do: {b, a}
  def isequal(a, a), do: true
  def isequal(_, _), do: false
end
